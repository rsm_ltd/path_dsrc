//************************************************************************************************************
//
// © 2016-2017 Regents of the University of California on behalf of the University of California at Berkeley
//       with rights granted for USDOT OSADP distribution with the ECL-2.0 open source license.
//
//*************************************************************************************************************
/*
 * Generated by asn1c-0.9.29 (http://lionet.info/asn1c)
 * From ASN.1 module "SEMI"
 * 	found in "../j2735_asn/SEMI_v2.3.0_070616.asn"
 * 	`asn1c -fcompound-names -gen-PER -gen-OER -pdu=auto`
 */

#include "Weather.h"

static asn_TYPE_member_t asn_MBR_wipers_2[] = {
	{ ATF_NOFLAGS, 0, offsetof(struct Weather__wipers, statFrnt),
		(ASN_TAG_CLASS_CONTEXT | (0 << 2)),
		-1,	/* IMPLICIT tag at current level */
		&asn_DEF_WiperStatus,
		0,
		{ 0, 0, 0 },
		0, 0, /* No default value */
		"statFrnt"
		},
	{ ATF_POINTER, 3, offsetof(struct Weather__wipers, rateFrnt),
		(ASN_TAG_CLASS_CONTEXT | (1 << 2)),
		-1,	/* IMPLICIT tag at current level */
		&asn_DEF_WiperRate,
		0,
		{ 0, 0, 0 },
		0, 0, /* No default value */
		"rateFrnt"
		},
	{ ATF_POINTER, 2, offsetof(struct Weather__wipers, statRear),
		(ASN_TAG_CLASS_CONTEXT | (2 << 2)),
		-1,	/* IMPLICIT tag at current level */
		&asn_DEF_WiperStatus,
		0,
		{ 0, 0, 0 },
		0, 0, /* No default value */
		"statRear"
		},
	{ ATF_POINTER, 1, offsetof(struct Weather__wipers, rateRear),
		(ASN_TAG_CLASS_CONTEXT | (3 << 2)),
		-1,	/* IMPLICIT tag at current level */
		&asn_DEF_WiperRate,
		0,
		{ 0, 0, 0 },
		0, 0, /* No default value */
		"rateRear"
		},
};
static const int asn_MAP_wipers_oms_2[] = { 1, 2, 3 };
static const ber_tlv_tag_t asn_DEF_wipers_tags_2[] = {
	(ASN_TAG_CLASS_CONTEXT | (0 << 2)),
	(ASN_TAG_CLASS_UNIVERSAL | (16 << 2))
};
static const asn_TYPE_tag2member_t asn_MAP_wipers_tag2el_2[] = {
    { (ASN_TAG_CLASS_CONTEXT | (0 << 2)), 0, 0, 0 }, /* statFrnt */
    { (ASN_TAG_CLASS_CONTEXT | (1 << 2)), 1, 0, 0 }, /* rateFrnt */
    { (ASN_TAG_CLASS_CONTEXT | (2 << 2)), 2, 0, 0 }, /* statRear */
    { (ASN_TAG_CLASS_CONTEXT | (3 << 2)), 3, 0, 0 } /* rateRear */
};
static asn_SEQUENCE_specifics_t asn_SPC_wipers_specs_2 = {
	sizeof(struct Weather__wipers),
	offsetof(struct Weather__wipers, _asn_ctx),
	asn_MAP_wipers_tag2el_2,
	4,	/* Count of tags in the map */
	asn_MAP_wipers_oms_2,	/* Optional members */
	3, 0,	/* Root/Additions */
	-1,	/* First extension addition */
};
static /* Use -fall-defs-global to expose */
asn_TYPE_descriptor_t asn_DEF_wipers_2 = {
	"wipers",
	"wipers",
	&asn_OP_SEQUENCE,
	asn_DEF_wipers_tags_2,
	sizeof(asn_DEF_wipers_tags_2)
		/sizeof(asn_DEF_wipers_tags_2[0]) - 1, /* 1 */
	asn_DEF_wipers_tags_2,	/* Same as above */
	sizeof(asn_DEF_wipers_tags_2)
		/sizeof(asn_DEF_wipers_tags_2[0]), /* 2 */
	{ 0, 0, SEQUENCE_constraint },
	asn_MBR_wipers_2,
	4,	/* Elements count */
	&asn_SPC_wipers_specs_2	/* Additional specs */
};

static asn_TYPE_member_t asn_MBR_weatherReport_9[] = {
	{ ATF_NOFLAGS, 0, offsetof(struct Weather__weatherReport, isRaining),
		(ASN_TAG_CLASS_CONTEXT | (0 << 2)),
		-1,	/* IMPLICIT tag at current level */
		&asn_DEF_EssPrecipYesNo,
		0,
		{ 0, 0, 0 },
		0, 0, /* No default value */
		"isRaining"
		},
	{ ATF_POINTER, 4, offsetof(struct Weather__weatherReport, rainRate),
		(ASN_TAG_CLASS_CONTEXT | (1 << 2)),
		-1,	/* IMPLICIT tag at current level */
		&asn_DEF_EssPrecipRate,
		0,
		{ 0, 0, 0 },
		0, 0, /* No default value */
		"rainRate"
		},
	{ ATF_POINTER, 3, offsetof(struct Weather__weatherReport, precipSituation),
		(ASN_TAG_CLASS_CONTEXT | (2 << 2)),
		-1,	/* IMPLICIT tag at current level */
		&asn_DEF_EssPrecipSituation,
		0,
		{ 0, 0, 0 },
		0, 0, /* No default value */
		"precipSituation"
		},
	{ ATF_POINTER, 2, offsetof(struct Weather__weatherReport, solarRadiation),
		(ASN_TAG_CLASS_CONTEXT | (3 << 2)),
		-1,	/* IMPLICIT tag at current level */
		&asn_DEF_EssSolarRadiation,
		0,
		{ 0, 0, 0 },
		0, 0, /* No default value */
		"solarRadiation"
		},
	{ ATF_POINTER, 1, offsetof(struct Weather__weatherReport, friction),
		(ASN_TAG_CLASS_CONTEXT | (4 << 2)),
		-1,	/* IMPLICIT tag at current level */
		&asn_DEF_EssMobileFriction,
		0,
		{ 0, 0, 0 },
		0, 0, /* No default value */
		"friction"
		},
};
static const int asn_MAP_weatherReport_oms_9[] = { 1, 2, 3, 4 };
static const ber_tlv_tag_t asn_DEF_weatherReport_tags_9[] = {
	(ASN_TAG_CLASS_CONTEXT | (3 << 2)),
	(ASN_TAG_CLASS_UNIVERSAL | (16 << 2))
};
static const asn_TYPE_tag2member_t asn_MAP_weatherReport_tag2el_9[] = {
    { (ASN_TAG_CLASS_CONTEXT | (0 << 2)), 0, 0, 0 }, /* isRaining */
    { (ASN_TAG_CLASS_CONTEXT | (1 << 2)), 1, 0, 0 }, /* rainRate */
    { (ASN_TAG_CLASS_CONTEXT | (2 << 2)), 2, 0, 0 }, /* precipSituation */
    { (ASN_TAG_CLASS_CONTEXT | (3 << 2)), 3, 0, 0 }, /* solarRadiation */
    { (ASN_TAG_CLASS_CONTEXT | (4 << 2)), 4, 0, 0 } /* friction */
};
static asn_SEQUENCE_specifics_t asn_SPC_weatherReport_specs_9 = {
	sizeof(struct Weather__weatherReport),
	offsetof(struct Weather__weatherReport, _asn_ctx),
	asn_MAP_weatherReport_tag2el_9,
	5,	/* Count of tags in the map */
	asn_MAP_weatherReport_oms_9,	/* Optional members */
	4, 0,	/* Root/Additions */
	-1,	/* First extension addition */
};
static /* Use -fall-defs-global to expose */
asn_TYPE_descriptor_t asn_DEF_weatherReport_9 = {
	"weatherReport",
	"weatherReport",
	&asn_OP_SEQUENCE,
	asn_DEF_weatherReport_tags_9,
	sizeof(asn_DEF_weatherReport_tags_9)
		/sizeof(asn_DEF_weatherReport_tags_9[0]) - 1, /* 1 */
	asn_DEF_weatherReport_tags_9,	/* Same as above */
	sizeof(asn_DEF_weatherReport_tags_9)
		/sizeof(asn_DEF_weatherReport_tags_9[0]), /* 2 */
	{ 0, 0, SEQUENCE_constraint },
	asn_MBR_weatherReport_9,
	5,	/* Elements count */
	&asn_SPC_weatherReport_specs_9	/* Additional specs */
};

asn_TYPE_member_t asn_MBR_Weather_1[] = {
	{ ATF_POINTER, 4, offsetof(struct Weather, wipers),
		(ASN_TAG_CLASS_CONTEXT | (0 << 2)),
		0,
		&asn_DEF_wipers_2,
		0,
		{ 0, 0, 0 },
		0, 0, /* No default value */
		"wipers"
		},
	{ ATF_POINTER, 3, offsetof(struct Weather, airTemp),
		(ASN_TAG_CLASS_CONTEXT | (1 << 2)),
		-1,	/* IMPLICIT tag at current level */
		&asn_DEF_AmbientAirTemperature,
		0,
		{ 0, 0, 0 },
		0, 0, /* No default value */
		"airTemp"
		},
	{ ATF_POINTER, 2, offsetof(struct Weather, airPres),
		(ASN_TAG_CLASS_CONTEXT | (2 << 2)),
		-1,	/* IMPLICIT tag at current level */
		&asn_DEF_AmbientAirPressure,
		0,
		{ 0, 0, 0 },
		0, 0, /* No default value */
		"airPres"
		},
	{ ATF_POINTER, 1, offsetof(struct Weather, weatherReport),
		(ASN_TAG_CLASS_CONTEXT | (3 << 2)),
		0,
		&asn_DEF_weatherReport_9,
		0,
		{ 0, 0, 0 },
		0, 0, /* No default value */
		"weatherReport"
		},
};
static const int asn_MAP_Weather_oms_1[] = { 0, 1, 2, 3 };
static const ber_tlv_tag_t asn_DEF_Weather_tags_1[] = {
	(ASN_TAG_CLASS_UNIVERSAL | (16 << 2))
};
static const asn_TYPE_tag2member_t asn_MAP_Weather_tag2el_1[] = {
    { (ASN_TAG_CLASS_CONTEXT | (0 << 2)), 0, 0, 0 }, /* wipers */
    { (ASN_TAG_CLASS_CONTEXT | (1 << 2)), 1, 0, 0 }, /* airTemp */
    { (ASN_TAG_CLASS_CONTEXT | (2 << 2)), 2, 0, 0 }, /* airPres */
    { (ASN_TAG_CLASS_CONTEXT | (3 << 2)), 3, 0, 0 } /* weatherReport */
};
asn_SEQUENCE_specifics_t asn_SPC_Weather_specs_1 = {
	sizeof(struct Weather),
	offsetof(struct Weather, _asn_ctx),
	asn_MAP_Weather_tag2el_1,
	4,	/* Count of tags in the map */
	asn_MAP_Weather_oms_1,	/* Optional members */
	4, 0,	/* Root/Additions */
	4,	/* First extension addition */
};
asn_TYPE_descriptor_t asn_DEF_Weather = {
	"Weather",
	"Weather",
	&asn_OP_SEQUENCE,
	asn_DEF_Weather_tags_1,
	sizeof(asn_DEF_Weather_tags_1)
		/sizeof(asn_DEF_Weather_tags_1[0]), /* 1 */
	asn_DEF_Weather_tags_1,	/* Same as above */
	sizeof(asn_DEF_Weather_tags_1)
		/sizeof(asn_DEF_Weather_tags_1[0]), /* 1 */
	{ 0, 0, SEQUENCE_constraint },
	asn_MBR_Weather_1,
	4,	/* Elements count */
	&asn_SPC_Weather_specs_1	/* Additional specs */
};

