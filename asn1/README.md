# About
This directory includes *.[ch]* files generated by the open source ASN.1 compiler '[asn1c](http://lionet.info/asn1c/compiler.html)'. It includes SAE J2735 message sets defined in version J2735_201603_ASN, and supports ASN.1 Unaligned Packed Encoding Rules (UPER).

# SAE J2735 DSRC Message Set Dictionary
The J2735 PDF of DSRC message set dictionary and the ASN files are available at [SAE](http://www.sae.org/standardsdev/dsrc/).

# ASN.1 Compiler
Users do not need to install the aforementioned 'asn1c' package. Message sets defined in J2735_201603_ASN have already been converted to *.[ch]* files and saved in the `include` and `src` subdirectories, respectively.

If interested, 'asn1c' package is available on [GitHub](https://github.com/vlm/asn1c). Additional information regarding 'asn1c' is available at [asn1c - Open Source ASN.1 Compiler](http://lionet.info/asn1c/compiler.html).

# Build and Install
- This directory is included in the upper level (`PATH_DSRC`) Makefile and does not need to build manually.
- After the compilation process, a shared library (`libasn.so.1`) is created in the `../lib` subdirectory.
- Users do not directly use the `libasn.so.1` library for UPER encoding and decoding of SAE J2735 messages, but rather using APIs provided in the `asn1j2735` directory.
