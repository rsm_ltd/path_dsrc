//************************************************************************************************************
//
// © 2016-2017 Regents of the University of California on behalf of the University of California at Berkeley
//       with rights granted for USDOT OSADP distribution with the ECL-2.0 open source license.
//
//*************************************************************************************************************
/*
 * Generated by asn1c-0.9.29 (http://lionet.info/asn1c)
 * From ASN.1 module "SEMI"
 * 	found in "../j2735_asn/SEMI_v2.3.0_070616.asn"
 * 	`asn1c -fcompound-names -gen-PER -gen-OER -pdu=auto`
 */

#ifndef	_IPv4Address_H_
#define	_IPv4Address_H_


#include <asn_application.h>

/* Including external dependencies */
#include <OCTET_STRING.h>

#ifdef __cplusplus
extern "C" {
#endif

/* IPv4Address */
typedef OCTET_STRING_t	 IPv4Address_t;

/* Implementation */
extern asn_per_constraints_t asn_PER_type_IPv4Address_constr_1;
extern asn_TYPE_descriptor_t asn_DEF_IPv4Address;
asn_struct_free_f IPv4Address_free;
asn_struct_print_f IPv4Address_print;
asn_constr_check_f IPv4Address_constraint;
ber_type_decoder_f IPv4Address_decode_ber;
der_type_encoder_f IPv4Address_encode_der;
xer_type_decoder_f IPv4Address_decode_xer;
xer_type_encoder_f IPv4Address_encode_xer;
oer_type_decoder_f IPv4Address_decode_oer;
oer_type_encoder_f IPv4Address_encode_oer;
per_type_decoder_f IPv4Address_decode_uper;
per_type_encoder_f IPv4Address_encode_uper;

#ifdef __cplusplus
}
#endif

#endif	/* _IPv4Address_H_ */
#include <asn_internal.h>
