//************************************************************************************************************
//
// © 2016-2017 Regents of the University of California on behalf of the University of California at Berkeley
//       with rights granted for USDOT OSADP distribution with the ECL-2.0 open source license.
//
//*************************************************************************************************************
/*
 * Generated by asn1c-0.9.29 (http://lionet.info/asn1c)
 * From ASN.1 module "SEMI"
 * 	found in "../j2735_asn/SEMI_v2.3.0_070616.asn"
 * 	`asn1c -fcompound-names -gen-PER -gen-OER -pdu=auto`
 */

#ifndef	_TimeToLive_H_
#define	_TimeToLive_H_


#include <asn_application.h>

/* Including external dependencies */
#include <NativeEnumerated.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Dependencies */
typedef enum TimeToLive {
	TimeToLive_minute	= 0,
	TimeToLive_halfHour	= 1,
	TimeToLive_day	= 2,
	TimeToLive_week	= 3,
	TimeToLive_month	= 4,
	TimeToLive_year	= 5
} e_TimeToLive;

/* TimeToLive */
typedef long	 TimeToLive_t;

/* Implementation */
extern asn_per_constraints_t asn_PER_type_TimeToLive_constr_1;
extern asn_TYPE_descriptor_t asn_DEF_TimeToLive;
extern const asn_INTEGER_specifics_t asn_SPC_TimeToLive_specs_1;
asn_struct_free_f TimeToLive_free;
asn_struct_print_f TimeToLive_print;
asn_constr_check_f TimeToLive_constraint;
ber_type_decoder_f TimeToLive_decode_ber;
der_type_encoder_f TimeToLive_encode_der;
xer_type_decoder_f TimeToLive_decode_xer;
xer_type_encoder_f TimeToLive_encode_xer;
oer_type_decoder_f TimeToLive_decode_oer;
oer_type_encoder_f TimeToLive_encode_oer;
per_type_decoder_f TimeToLive_decode_uper;
per_type_encoder_f TimeToLive_encode_uper;

#ifdef __cplusplus
}
#endif

#endif	/* _TimeToLive_H_ */
#include <asn_internal.h>
