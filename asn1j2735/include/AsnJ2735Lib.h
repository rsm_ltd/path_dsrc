//************************************************************************************************************
//
// © 2016-2017 Regents of the University of California on behalf of the University of California at Berkeley
//       with rights granted for USDOT OSADP distribution with the ECL-2.0 open source license.
//
//*************************************************************************************************************
#ifndef _ASN_J2735_LIB_H
#define _ASN_J2735_LIB_H

#include <cstddef>
#include <cstdint>
#include <fstream>

#include "dsrcFrame.h"

namespace AsnJ2735Lib
{ /// UPER encoding functions
	size_t encode_msgFrame(const Frame_element_t& dsrcFrameIn, uint8_t* buf, size_t size);
	/// UPER decoding functions
	size_t decode_msgFrame(const uint8_t* buf, size_t size, Frame_element_t& dsrcFrameOut);
	///
	void print_bsmdata(std::ostream& OS, const BSM_element_t& bsm);
	void print_srmdata(std::ostream& OS, const SRM_element_t& srm);
	void print_ssmdata(std::ostream& OS, const SSM_element_t& ssm);
	void print_mapdata(std::ostream& OS, const MapData_element_t& mapData);
	void print_spatdata(std::ostream& OS, const SPAT_element_t& spat);
};

#endif
