# About
This directory includes C++11 source code which provide library APIs for UPER (Unaligned Packed Encoding Rules) encoding and decoding of J2735 messages, including
- Basic Safety Message (BSM)
- Signal Phase and Timing Message (SPaT)
- MAP message
- Signal Request Message (SRM), and
- Signal Status Message (SSM)

# Build and Install
- This directory is included in the upper level (`PATH_DSRC`) Makefile and does not need to build manually.
- After the compilation process, a dynamic shared library (`libdsrc.so.1`) is created in the `../lib` subdirectory.
- User's applications links the `libdsrc.so` to access the UPER encoding and decoding library APIs.

# Usage of UPER Encoding/Decoding APIs
- Two APIs are provided, namely, `encode_msgFrame` for UPER encoding and `decode_msgFrame` for UPER decoding.
- Definitions of `encode_msgFrame` and `decode_msgFrame` API interface are defined in `AsnJ2735Lib.h`.
- UPER encoding and decoding supports the 5 aforementioned messages (i.e., BSM, SSM, MAP, SPaT, and SRM).

## UPER Encoding
- `size_t encode_msgFrame(const Frame_element_t& dsrcFrameIn, uint8_t* buf, size_t size)`
- Input
	- User to fill `dsrcFrameIn` structure (defined in `dsrcFrame.h`)
- Output
	- The encoded byte array are placed and returned in `buf` with length `size`; and
	- The number of bytes of the encoded byte array (which should be less than `size`. Set `size = 2000` will be sufficient for all messages).

## UPER Decoding
- `size_t decode_msgFrame(const uint8_t* buf, size_t size, Frame_element_t& dsrcFrameOut)`
- Input
	- User to provide encoded byte array in `buf` with length `size`.
- Output
	- The decoded messages are filled and returned in `dsrcFrameOut`; and
	- The number of bytes in `buf` that have used for decoding.

## Description of Frame_element_t
The definition of message structure (BSM, SRM, MAP, SPaT, and SSM) is defined in `dsrcBSM.h`, `dsrcSRM.h`, `dsrcMapData.h`, `dsrcSPAT.h`, and `dsrcSSM.h`, respectively.

To have an uniform API for UPER encoding and decoding, the data structure `Frame_element_t` is used to include the 5 targeted DSRC messages.

	struct Frame_element_t
	{
		uint16_t dsrcMsgId;
		MapData_element_t mapData;
		SPAT_element_t    spat;
		BSM_element_t     bsm;
		SRM_element_t     srm;
		SSM_element_t     ssm;
		void reset(void)
		{
			dsrcMsgId = MsgEnum::DSRCmsgID_unknown;
			mapData.reset();
			spat.reset();
			bsm.reset();
			srm.reset();
			ssm.reset();
		};
	};

When encoding a message, using BSM as an example, the user
- declare `uint8_t buf[2000]`, which to hold the encoded byte array
- declare `struct Frame_element_t dsrcFrameIn`
- fill `dsrcFrameIn.bsm` with user data
- set `dsrcFrameIn.dsrcMsgId = MsgEnum::DSRCmsgID_bsm`
- call `encode_msgFrame`
- `buf` is then filled with the encoded byte array

When decoding a message, using BSM as an example, the user
- declare `struct Frame_element_t dsrcFrameOut`
- call `decode_msgFrame` with the encoded byte array as input
- `dsrcFrameOut` is then filled with the decoded message structure, the type of decoded message is specified by `dsrcFrameOut.dsrcMsgId`.
- DSRC Message ID is defined in `msgEnum.h`.
- Help APIs for print the encoded message are included as `print_xxxdata`, defined in `AsnJ2735Lib.h`
