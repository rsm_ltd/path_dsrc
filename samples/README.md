# About
This directory includes C++11 source code for sample programs.

## `testDecoder`
`testDecoder` checks the UPER encoder and decoder with hard-coded data elements. 

It uses the hard-coded data elements to encode a DSRC message (payload), then decode the encoded payload back to message structure.

It also logs the decoded message structure into a data file `testDecoder_xxx.out` for the comparison with the hard-coded data elements.
 
DSRC messages can be tested include
- BSM
- SRM
- SPaT
- SSM

# Build and Install
- This directory is included in the upper level (`PATH_DSRC`) Makefile.
- After the compilation process, an executable binary file (`testDecoder`) is created in the `../bin` subdirectory.
- When adding new sample programs, users can change the `Makefile` accordingly to include the build and install of new sample programs. 

# Usage

## `testDecoder`
- In `PATH_DSRC/bin` directory, run `./testDecoder -s <BSM|SRM|SPaT|SSM>`
- Status of message encoding and decoding will be displayed on the computer screen.
- Check the output file `testDecoder_<BSM|SRM|SPaT|SSM>.out` and compare to the hard-coded data elements for correctness.
