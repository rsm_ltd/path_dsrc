//*************************************************************************************************************
//
// © 2016-2017 Regents of the University of California on behalf of the University of California at Berkeley
//       with rights granted for USDOT OSADP distribution with the ECL-2.0 open source license.
//
//*************************************************************************************************************
/* testDecoder.cpp
 * testDecoder tests the DSRC message encoder and decoder with hard-coded data elements.
 * DSRC messages can be tested include: BSM, SRM, SPaT, and SSM.
 *
 * testDecoder encodes the payload with hard-coded data elements, decodes the payload, and compare the results
 * with the hard-coded inputs.
 *
 * Usage: testDecoder -s <BSM|SRM|SPaT|SSM>
 *
 */

#include <cstddef>
#include <cstdlib>
#include <cmath>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <unistd.h>

#include "AsnJ2735Lib.h"

void do_usage(const char* progname)
{
	std::cerr << "Usage" << progname << std::endl;
	std::cerr << "\t-s scenario: BSM, SRM, SPaT, SSM" << std::endl;
	std::cerr << "\t-? print this message" << std::endl;
	exit(EXIT_FAILURE);
}

int main(int argc, char** argv)
{
	int option;
	std::string scenario;

	while ((option = getopt(argc, argv, "s:?")) != EOF)
	{
		switch(option)
		{
		case 's':
			scenario = std::string(optarg);
			break;
		case '?':
		default:
			do_usage(argv[0]);
			break;
		}
	}
	if (scenario.empty() || ((scenario.compare("BSM") != 0) && (scenario.compare("SRM") != 0)
			&& (scenario.compare("SPaT") != 0) && (scenario.compare("SSM") != 0)))
		do_usage(argv[0]);

	/// parameters
	const uint16_t regionalId = 0;
	const uint16_t intersectionId = 1003;   /// page-mill
	const uint32_t vehId = 601;
	const uint8_t  priorityLevel = 5;
	const uint8_t  inLaneId = 8;       /// page-mill, northbound ECR, inbound middle through lane
	const uint8_t  outLaneId = 30;     /// page-mill, northbound ECR, outbound middle through lane
	const uint16_t heading = (uint16_t)(std::round(60.0 / 0.0125));       /// 60.0 degree
	const uint16_t speed = (uint16_t)(std::round(35 * 0.277778 / 0.02));  /// 35 KPH
	int32_t latitude  = 374230638;
	int32_t longitude = -1221420467;
	int32_t elevation = 126;
	const uint32_t time2go = 30000;    /// 30 seconds
	const uint16_t duration = 2000;    /// 2 seconds
	const uint32_t minuteOfYear = 56789;
	const uint16_t msOfMinute = 12345;
	uint32_t ETAsec = time2go;
	/// buffer to hold message payload
	size_t bufSize = 2000;
	std::vector<uint8_t> buf(bufSize, 0);
	/// dsrcFrameOut to store UPER decoding result
	Frame_element_t dsrcFrameOut;
	/// dsrcFrameIn to store input to UPER encoding function
	Frame_element_t dsrcFrameIn;
	dsrcFrameIn.reset();
	/// output log file
	std::string fout = "testDecoder_" + scenario + ".out";
	std::ofstream OS_OUT(fout);

	std::cout << "Test " << scenario << " encode and decode" << std::endl;

	if (scenario.compare("BSM") == 0)
	{	/// manual input bsmIn
		dsrcFrameIn.dsrcMsgId = MsgEnum::DSRCmsgID_bsm;
		BSM_element_t& bsmIn = dsrcFrameIn.bsm;
		bsmIn.reset();
		bsmIn.msgCnt = 1;
		bsmIn.id = vehId;
		bsmIn.timeStampSec = msOfMinute;
		bsmIn.latitude  = latitude;
		bsmIn.longitude = longitude;
		bsmIn.elevation = elevation;
		bsmIn.yawRate   = 0;
		bsmIn.vehLen    = 1200;
		bsmIn.vehWidth  = 300;
		bsmIn.speed     = speed;
		bsmIn.heading   = heading;
		/// encode BSM payload
		size_t payload_size = AsnJ2735Lib::encode_msgFrame(dsrcFrameIn, &buf[0], bufSize);
		if (payload_size > 0)
		{
			std::cout << "encode_bsm_payload succeed" << std::endl;
			/// decode BSM payload
			if ((AsnJ2735Lib::decode_msgFrame(&buf[0], payload_size, dsrcFrameOut) > 0)
				&& (dsrcFrameOut.dsrcMsgId == MsgEnum::DSRCmsgID_bsm))
			{
				BSM_element_t& bsmOut = dsrcFrameOut.bsm;
				std::cout << "decode_msgFrame for BSM succeed" << std::endl;
				OS_OUT << "Payload size " << payload_size << std::endl;
				AsnJ2735Lib::print_bsmdata(OS_OUT, bsmOut);
			}
			else
				std::cout << "Failed decode_msgFrame for BSM" << std::endl;
		}
		else
			std::cout << "Failed encode_msgFrame for BSM" << std::endl;
		std::cout << "Done test BSM encode and decode" << std::endl << std::endl;
	}
	else if (scenario.compare("SRM") == 0)
	{	/// manual input srmIn
		dsrcFrameIn.dsrcMsgId = MsgEnum::DSRCmsgID_srm;
		SRM_element_t& srmIn = dsrcFrameIn.srm;
		srmIn.reset();
		srmIn.timeStampMinute = minuteOfYear;
		srmIn.timeStampSec = msOfMinute;
		srmIn.msgCnt = 2;
		srmIn.regionalId = regionalId;
		srmIn.intId = intersectionId;
		srmIn.reqId = priorityLevel;
		srmIn.inApprochId = 0;
		srmIn.inLaneId = inLaneId;
		srmIn.outApproachId = 0;
		srmIn.outLaneId = outLaneId;
		srmIn.ETAsec    = static_cast<uint16_t>((ETAsec % 60000) & 0xFFFF);
		srmIn.ETAminute = static_cast<uint32_t>(minuteOfYear + std::floor(ETAsec / 60000));
		srmIn.duration  = duration;
		srmIn.vehId     = vehId;
		srmIn.latitude  = latitude;
		srmIn.longitude = longitude;
		srmIn.elevation = elevation;
		srmIn.heading   = heading;
		srmIn.speed     = speed;
		srmIn.reqType   = MsgEnum::requestType::priorityRequest;
		srmIn.vehRole   = MsgEnum::basicRole::transit;
		srmIn.vehType   = MsgEnum::vehicleType::bus;
		/// encode SRM payload
		size_t payload_size = AsnJ2735Lib::encode_msgFrame(dsrcFrameIn, &buf[0], bufSize);
		if (payload_size > 0)
		{
			std::cout << "encode_msgFrame for SRM succeed" << std::endl;
			/// decode SRM payload
			if ((AsnJ2735Lib::decode_msgFrame(&buf[0], payload_size, dsrcFrameOut) > 0)
				&& (dsrcFrameOut.dsrcMsgId == MsgEnum::DSRCmsgID_srm))
			{
				SRM_element_t& srmOut = dsrcFrameOut.srm;
				std::cout << "decode_msgFrame for SRM succeed" << std::endl;
				OS_OUT << "Payload size " << payload_size << std::endl;
				AsnJ2735Lib::print_srmdata(OS_OUT, srmOut);
			}
			else
				std::cout << "Failed decode_msgFrame for SRM" << std::endl;
		}
		else
			std::cout << "Failed encode_msgFrame for SRM" << std::endl;
		std::cout << "Done test SRM encode and decode" << std::endl << std::endl;
	}
	else if (scenario.compare("SPaT") == 0)
	{	/// manual input spatIn
		dsrcFrameIn.dsrcMsgId = MsgEnum::DSRCmsgID_spat;
		SPAT_element_t& spatIn = dsrcFrameIn.spat;
		spatIn.reset();
		spatIn.regionalId = regionalId;
		spatIn.id = intersectionId;
		spatIn.msgCnt = 3;
		spatIn.timeStampMinute = minuteOfYear;
		spatIn.timeStampSec = msOfMinute;
		spatIn.permittedPhases.set();          /// all 8 phases permitted
		for (size_t i = 0; i < 8; i++)
		{
			if (i % 2 == 1)
				spatIn.permittedPedPhases.set(i);  /// even phase # is pedestrian phase
		}
		spatIn.status.reset();
		uint16_t currentTime = static_cast<uint16_t>((minuteOfYear % 60) * 600 + msOfMinute / 100);
		uint16_t baseTime = 50;
		uint16_t stepTime = 50;
		std::cout << "tenths of a second in the current hour: " << currentTime << std::endl;
		for (auto& phaseState : spatIn.phaseState)
		{
			phaseState.currState = MsgEnum::phaseState::redLight;
			phaseState.startTime = baseTime;
			phaseState.minEndTime = (uint16_t)(phaseState.startTime + stepTime);
			phaseState.maxEndTime = (uint16_t)(phaseState.minEndTime + stepTime);
			baseTime = (uint16_t)(phaseState.minEndTime +  stepTime);
		}
		spatIn.phaseState[1].currState = MsgEnum::phaseState::protectedGreen;
		spatIn.phaseState[5].currState = MsgEnum::phaseState::protectedGreen;
		for (size_t i = 0; i < 8; i++)
		{
			if (i % 2 == 0)
				continue;
			auto& phaseState = spatIn.pedPhaseState[i];
			phaseState.currState = MsgEnum::phaseState::redLight;
			phaseState.startTime = baseTime;
			phaseState.minEndTime = (uint16_t)(phaseState.startTime + stepTime);
			phaseState.maxEndTime = (uint16_t)(phaseState.minEndTime + stepTime);
			baseTime = (uint16_t)(phaseState.minEndTime +  stepTime);
		}
		spatIn.pedPhaseState[1].currState = MsgEnum::phaseState::protectedYellow;
		spatIn.pedPhaseState[5].currState = MsgEnum::phaseState::protectedYellow;
		/// encode SPaT payload
		size_t payload_size = AsnJ2735Lib::encode_msgFrame(dsrcFrameIn, &buf[0], bufSize);
		if (payload_size > 0)
		{
			std::cout << "encode_spat_payload succeed" << std::endl;
			/// decode SPaT payload
			if ((AsnJ2735Lib::decode_msgFrame(&buf[0], payload_size, dsrcFrameOut) > 0)
				&& (dsrcFrameOut.dsrcMsgId == MsgEnum::DSRCmsgID_spat))
			{
				SPAT_element_t& spatOut = dsrcFrameOut.spat;
				std::cout << "decode_msgFrame for SPaT succeed" << std::endl;
				OS_OUT << "Payload size " << payload_size << std::endl;
				AsnJ2735Lib::print_spatdata(OS_OUT, spatOut);
			}
			else
				std::cout << "Failed decode_msgFrame for SPaT" << std::endl;
		}
		else
			std::cout << "Failed encode_msgFrame for SPaT" << std::endl;
		std::cout << "Done test SPaT encode and decode" << std::endl << std::endl;
	}
	else if (scenario.compare("SSM") == 0)
	{	/// manual input ssmIn
		dsrcFrameIn.dsrcMsgId = MsgEnum::DSRCmsgID_ssm;
		SSM_element_t& ssmIn = dsrcFrameIn.ssm;
		ssmIn.reset();
		ssmIn.timeStampMinute = minuteOfYear;
		ssmIn.timeStampSec = msOfMinute;
		ssmIn.msgCnt = 4;
		ssmIn.updateCnt = 1;
		ssmIn.regionalId = regionalId;
		ssmIn.id = intersectionId;
		SignalRequetStatus_t requestStatus;
		requestStatus.reset();
		requestStatus.vehId = vehId;
		requestStatus.reqId = priorityLevel;
		requestStatus.sequenceNumber = 2;
		requestStatus.vehRole   = MsgEnum::basicRole::transit;
		requestStatus.inLaneId  = inLaneId;
		requestStatus.outLaneId = outLaneId;
		requestStatus.ETAminute = static_cast<uint32_t>(minuteOfYear + std::floor(ETAsec / 60000));
		requestStatus.ETAsec    = static_cast<uint16_t>((ETAsec % 60000) & 0xFFFF);
		requestStatus.duration  = duration;
		requestStatus.status    = MsgEnum::requestStatus::granted;
		ssmIn.mpSignalRequetStatus.push_back(requestStatus);
		/// encode SSM payload
		size_t payload_size = AsnJ2735Lib::encode_msgFrame(dsrcFrameIn, &buf[0], bufSize);
		if (payload_size > 0)
		{
			std::cout << "encode_ssm_payload succeed" << std::endl;
			/// decode SSM payload
			if ((AsnJ2735Lib::decode_msgFrame(&buf[0], payload_size, dsrcFrameOut) > 0)
				&& (dsrcFrameOut.dsrcMsgId == MsgEnum::DSRCmsgID_ssm))
			{
				SSM_element_t& ssmOut = dsrcFrameOut.ssm;
				std::cout << "decode_msgFrame for SSM succeed" << std::endl;
				OS_OUT << "Payload size " << payload_size << std::endl;
				AsnJ2735Lib::print_ssmdata(OS_OUT, ssmOut);
			}
			else
				std::cout << "Failed decode_msgFrame for SSM" << std::endl;
		}
		else
			std::cout << "Failed encode_msgFrame for SSM" << std::endl;
	}

	std::cout << "Done test " << scenario << " encode and decode" << std::endl;
	OS_OUT.close();
	return(0);
}
