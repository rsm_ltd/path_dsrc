# Makefile for 'PATH_DSRC' directory

ASN1_DIR    := asn1
J2735_DIR   := asn1j2735
Sample_DIR  := samples

.PHONY: all asn j2375 sample install

all: asn j2375 sample

asn:
	(cd $(ASN1_DIR); make clean; make all)

j2375:
	(cd $(J2735_DIR); make clean; make all)

sample:
	(cd $(Sample_DIR); make clean; make all)

install:
	(cd $(Sample_DIR); make install)

